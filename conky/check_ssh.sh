
_remote=$1

if [[ -z ${_remote} ]]
then
  echo "Error: No remote given."
  exit 1
fi

#ssh ${_remote} uptime > /dev/null 2>&1
echo 1

if [[ $? == 0 ]]
then
  echo "Online"
else
  echo "Offline"
fi


