#!/bin/bash

_proc=`ps aux | grep 'firewalld' | wc -l`

if [[ ${_proc} -lt 2 ]]
then
  echo "Disabled"
else
  echo "Active"
fi

